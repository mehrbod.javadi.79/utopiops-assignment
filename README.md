# Utopiops Assignment

Simple API to get repository tree of project from GitLab by Flask.


# Run the code

To use this application, first install Flask on your system.

`$ pip install flask`

Flask is compatible with Python 3.7+

Then run code with

`$ python3 app.py`

And now the server is running!

# APIs

First API takes `userId` and `gitlabAccessToken` and save it for user's next requests.

Second API provides repositories under the specified projects with requesting to GitLab API.

# Configuration

You can set server IP and server port in `config.json` as you desire.

# Main code

Dictionary `users` saves all `userId` and `gitlabAccessToken` from several users.
Both endpoints first check if the given data from user is correct, then complete the task.
