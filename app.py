from flask import Flask, request
from flask.json import jsonify
import requests
import json

app = Flask(__name__)

config = json.load(open('config.json',))
users = {}

@app.route('/', methods = ['GET'])
def home_page():
    return "Hello world!"

@app.route('/token', methods = ['POST'])
def token_api():
    body = request.get_json()
    if 'gitlabAccessToken' not in body or 'userId' not in body:
        return {'message': 'userId and gitlabAccessToken is required'}, 400

    users[body['userId']] = body['gitlabAccessToken']
    return {'message': 'OK'}, 200


@app.route('/projects/<project_id>/repository_tree', methods = ['GET'])
def projects_api(project_id):
    if 'userId' not in request.headers:
        return {'message': 'userId is required'}, 400
    
    user_id = request.headers['userId']
    if user_id not in users:
        return {'message': 'ERROR'}, 400
    
    response = requests.get(f'https://gitlab.com/api/v4/projects/{project_id}/repository/tree', headers = {'PRIVATE-TOKEN': users[user_id]})
    return jsonify(response.json()), 200


app.run(host = config['SERVER_IP'], port = config['SERVER_PORT'], debug = True)
